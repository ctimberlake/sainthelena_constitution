# The Constitution of Saint Helena

### Amendment 1
The Government of Saint Helena shall make no law respecting an establishment of religion, or prohibiting the free exercise thereof; or abridging the freedom of speech, or of the press; or the right of the people peaceably to assemble, and to petition the government for a redress of grievances.

### Amendment 2
The Government of Saint Helena shall make no law restricting the commerce of Island Corporations and Services. Corporations operating Foreign Enterprise may be restricted but not prohibited from Island Commerce.

### Amendment 3
The Government of Saint Helena shall provide free coffee every second Monday of the month. This shall be known as Coffeepalooza.